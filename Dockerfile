# build environment
FROM node:14.7.0-alpine as build
WORKDIR /app

# Build args
ARG NODE_ENV
ARG PORT
ARG REACT_APP_NOT_SECRET_CODE

# Environment vars
ENV NODE_ENV=$NODE_ENV
ENV PORT=$PORT
ENV REACT_APP_NOT_SECRET_CODE=$REACT_APP_NOT_SECRET_CODE

ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
RUN npm install --silent
RUN npm install react-scripts@3.0.1 -g --silent
COPY . /app
RUN npm run build

# production environment
FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d
EXPOSE 79
CMD ["nginx", "-g", "daemon off;"]
