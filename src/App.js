import React, { Component } from 'react';
import TodoList from './components/TodoList';
import './App.css';

class App extends Component {
  componentDidMount(){
    console.log('zip',process.env);
  }
  render() {
    return (
      <div className="todoapp">
        <h2>{process.env.REACT_APP_NOT_SECRET_CODE}</h2>
        <TodoList />
      </div>
    );
  }
}

export default App;
